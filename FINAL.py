"""""""""""""""""""""
Sara Grandone
Final Project- Getting to Mars
05/01/15

"""""""""""""""""""""

import numpy as np
import matplotlib.pyplot as plt
from delta import deltav
from delta import deltat
from red import red
from FinalPlotter import Trajectories
from FinalPlotter import dvVSr
from FinalPlotter import dtVSr
from FinalPlotter import dvdtVSr

plt.close('all')

### Define Constants To be Used (SI units)
G = 6.674e-11
M_Sun = 1.989e30
D_Earth = 1.496e11
D_Mars = 2.253e11
D_Ceres = 4.14e11
D_Jupiter = 5.88e11
D_Saturn = 1.433e12
mu_Sun = G*M_Sun
MaxTime = 365*50 # Won't allow longer trip than this
Maxdv = 16e3 # Won't allow larger dv than this due to tech constraints
it = 100 #iterations

################################

# CHANGE THIS PARAMETER to choose where we want to go
D = D_Mars
 
##############################   
       
    
#Will be useful for plotting routines- How many times larger is apogee
#   of transfer orbit from final orbit
Out = 20

### Calculate dv and t for both kinds of transfers
dvh = deltav(D,1,0)
dvbe = deltav(D,0,1)
dth,alphah = deltat(D,1,0)
dtbe,alphabe = deltat(D,0,1)

### Define Constraints
emin = (D_Mars-D_Earth)/(D_Mars+D_Earth)
emax = (D*Out-D_Earth)/(D*Out+D_Earth)
e_vec = np.linspace(emin,emax,it)
dist_vec = np.linspace(D_Earth,D*Out/2,it)
one_arr = np.ones([it,it])

### Generate Arrays for 2-variable dependance
e,r = np.meshgrid(e_vec,dist_vec)

### Reduce into vectors that take into account dv and t limits
dth_red, dvh_red, rh_red, eh_red = red(D,dvh,dth,Maxdv,MaxTime,1,0)
dtbe_red, dvbe_red, rbe_red, ebe_red = red(D,dvbe,dtbe,Maxdv,MaxTime,0,1)

### More conceptual scales       
r_AU = r/D_Earth
D_Earth_AU = 1
D_Mars_AU = D_Mars/D_Earth
D_Ceres_AU = D_Ceres/D_Earth
D_Jupiter_AU = D_Jupiter/D_Earth
D_Saturn_AU = D_Saturn/D_Earth
dvh_km = (dvh_red)/1.e3
dvbe_km = (dvbe_red)/1.e3
sizeh_red = (dvh_km.shape)[0]
sizebe_red = (dvbe_km.shape)[0]


### Plotting

Trajectories(D)

dvVSr(r,dvh,dvbe,dvh_red,dvbe_red,dvh_km,it-1,Maxdv)
dvVSr(r,dvh,dvbe,dvh_red,dvbe_red,dvh_km,it/2,Maxdv)
dvVSr(r,dvh,dvbe,dvh_red,dvbe_red,dvh_km,1,Maxdv)

dtVSr(r,dth,dtbe,dth_red,dtbe_red,it-1,MaxTime)
dtVSr(r,dth,dtbe,dth_red,dtbe_red,it/2,MaxTime)
dtVSr(r,dth,dtbe,dth_red,dtbe_red,1,MaxTime)

dvdtVSr(r,dvh,dvbe,dth,dtbe,dvh_red,dvbe_red,dth_red,dtbe_red,it-1)


## Print results
print('Hohmann Transfer Orbit is Optimal for Human Time Scale Trips \n')
print('Bi-elliptic is optimal for lower dv but at cost of Time \n')
print('Displacement plot is key because there are both a max time and max dv constraint, so it is puts an upper limit on the orbits')
