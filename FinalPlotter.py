import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Arc

def Trajectories(D):

    ########Trajectories###########################

    ### Define Constants (SI units)
    G = 6.674e-11
    M_Sun = 1.989e30
    D_Earth = 1.496e11
    mu_Sun = G*M_Sun
    it = 100 # Number of iterations
    D_Mars = 2.253e11
    
    ### Define Constraints
    Out = 20
    emax = (D*Out-D_Earth)/(D*Out+D_Earth)

    
    ##### Plot
    
# Create circular orbits
    plt.figure()
    ax = plt.gca()
    theta = np.linspace(0,2*np.pi,1000)
    x_e = np.cos(theta)*1
    y_e = np.sin(theta)*1
    x_j = np.cos(theta)*D/D_Earth
    y_j = np.sin(theta)*D/D_Earth

    plt.plot(0,0,'r*',label='Sun')
    plt.plot(x_e,y_e,'b',label='Earth Orbit')
    plt.plot(x_j,y_j,'y',label='Desired Orbit')

# Use equations to find elliptical transfer orbits
    a1 = np.mean([1,D/D_Earth])
    e1 = D/D_Earth/a1-1
    b1 = a1*np.sqrt(1-e1**2)
    a2 = D_Earth/(1-emax)/D_Earth
    b2 = a2*np.sqrt(1-emax**2)
    rap23 = a2*(1+emax)
    a3 = np.mean([rap23,D/D_Earth])
    e3 = rap23/a2-1
    b3 = a3*np.sqrt(1-e3**2)
    
    plt.plot(rap23,0,'kx')
    plt.plot(-D,0,'kx')

    center1 = np.mean([-1,D/D_Earth])
    center2 = np.mean([1,rap23])-1
    center3 = np.mean([D/D_Earth,rap23])-D/D_Earth

    ellipse1 = Arc(xy=(center1, 0), width=2*a1, height=2*b1, 
                            edgecolor='m', fc='None', lw=1,theta1=180,theta2=360,label='Hohmann')

    ellipse2 = Arc(xy=(center2, 0), width=2*a2, height=2*b2, 
                            edgecolor='r', fc='None', lw=1,theta1=180,theta2=360,label='BE First Ellipse')
    ellipse3 = Arc(xy=(center3, 0), width=2*a3, height=2*b3, 
                            edgecolor='g', fc='None', lw=1,theta1=0,theta2=180,label='BE Second Ellipse')

    ax.add_patch(ellipse1)
    ax.add_patch(ellipse2)
    ax.add_patch(ellipse3)
    plt.xlim(-D/D_Earth*1.3,1.1*(center2+a2))
    plt.ylim(-b2*1.3,b3)
    plt.legend(loc='lower right',numpoints=1)
    plt.xlabel('Distance (AU)')
    plt.ylabel('Distance (AU)')
    plt.show()



def dvVSr(r,dvh,dvbe,dvh_red,dvbe_red,dvh_km,index,Maxdv):

    ##### Define Some Things
    it = 100
    D_Earth = 1.496e11
    r_AU = r/D_Earth
    D_Mars = 2.253e11
    D_Ceres = 4.14e11
    D_Jupiter = 5.88e11
    D_Saturn = 1.433e12
    D_Mars_AU = D_Mars/D_Earth
    D_Ceres_AU = D_Ceres/D_Earth
    D_Jupiter_AU = D_Jupiter/D_Earth
    D_Saturn_AU = D_Saturn/D_Earth
    dvh_km = (dvh_red)/1.e3
    dvbe_km = (dvbe_red)/1.e3
    
    ### What kind of e?
    if index ==it-1:
        Name = 'High BE Eccentricity'
    if (index>1)  & (index<(it-1)):
        Name = 'Mid BE Eccentricity'
    if index <=1:
        Name = 'Low BE Eccentricity'

    ########dv vs. r###########################
    plt.figure()
    plt.plot(r_AU[:,index],dvh[:,index]/1.e3,'g',label='Hohmann') #Hohmann
    plt.plot(r_AU[:,index],dvbe[:,index]/1.e3,'b',label='Bi-elliptic') #Bi-elliptic
    plt.plot(D_Mars_AU*np.ones(dvh_km.shape),dvh_km,'r',linestyle='-.',label='Mars') #Mars orbit
    plt.plot(D_Ceres_AU*np.ones(dvh_km.shape),dvh_km,'c',linestyle='--',label='Ceres') #Ceres orbit
    plt.plot(D_Jupiter_AU*np.ones(dvh_km.shape),dvh_km,'y',linestyle='-',label='Jupiter') #Jupiter orbit
    plt.plot(D_Saturn_AU*np.ones(dvh_km.shape),dvh_km,'m',linestyle=':',label='Saturn') #Saturn orbit
    plt.plot(r_AU[:,index],Maxdv/1.e3*np.ones((r_AU[:,index]).shape),'k',label='dv limit')
    plt.xlabel('Outer Orbit Radius (AU)')
    plt.ylabel('dv (km/s)')
    plt.title(Name)
    plt.legend(loc='lower right',numpoints=1)
    plt.show()
    
    
def dtVSr(r,dth,dtbe,dth_red,dtbe_red,index,MaxTime):

    ##### Define Some Things
    it = 100
    D_Earth = 1.496e11
    r_AU = r/D_Earth
    D_Mars = 2.253e11
    D_Ceres = 4.14e11
    D_Jupiter = 5.88e11
    D_Saturn = 1.433e12
    D_Mars_AU = D_Mars/D_Earth
    D_Ceres_AU = D_Ceres/D_Earth
    D_Jupiter_AU = D_Jupiter/D_Earth
    D_Saturn_AU = D_Saturn/D_Earth

    
    ### What kind of e?
    if index ==it-1:
        Name = 'High BE Eccentricity'
    if (index>1)  & (index<(it-1)):
        Name = 'Mid BE Eccentricity'
    if index <=1:
        Name = 'Low BE Eccentricity'

    ########dv vs. t###########################
    plt.figure()
    plt.plot(r_AU[:,index],dth[:,index]/365,'g',label='Hohmann') #Hohmann
    plt.plot(r_AU[:,index],dtbe[:,index]/365,'b',label='Bi-elliptic') #Bi-elliptic
    plt.plot(D_Mars_AU*np.ones(dth_red.shape),dth_red/365,'r',linestyle='-.',label='Mars') #Mars orbit
    plt.plot(D_Ceres_AU*np.ones(dth_red.shape),dth_red/365,'c',linestyle='--',label='Ceres') #Ceres orbit
    plt.plot(D_Jupiter_AU*np.ones(dth_red.shape),dth_red/365,'y',linestyle='-',label='Jupiter') #Jupiter orbit
    plt.plot(D_Saturn_AU*np.ones(dth_red.shape),dth_red/365,'m',linestyle=':',label='Saturn') #Saturn orbit
    plt.plot(r_AU[:,index],MaxTime/365*np.ones((r_AU[:,index]).shape),'k',label='Time limit')
    plt.xlabel('Outer Orbit Radius (AU)')
    plt.ylabel('Time (years)')
    plt.title(Name)
    plt.legend(loc='lower right',numpoints=1)
    plt.show()
    
    
def dvdtVSr(r,dvh,dvbe,dth,dtbe,dvh_red,dvbe_red,dth_red,dtbe_red,index):
    
    ################### dv*dt vs. radius #######################
    #Plot without Constraints, high eccentricity
    
        ##### Define Some Things
    it = 100
    D_Earth = 1.496e11
    r_AU = r/D_Earth
    D_Mars = 2.253e11
    D_Ceres = 4.14e11
    D_Jupiter = 5.88e11
    D_Saturn = 1.433e12
    D_Mars_AU = D_Mars/D_Earth
    D_Ceres_AU = D_Ceres/D_Earth
    D_Jupiter_AU = D_Jupiter/D_Earth
    D_Saturn_AU = D_Saturn/D_Earth
    
    # Consider max time and max dv constraints
    index_h = np.where((dvh[:,index]*dth[:,index])<=np.max((dvh_red*dth_red)))
    index_be = np.where((dvbe[:,index]*dtbe[:,index])<=np.max((dvbe_red*dtbe_red)))


    plt.figure()
    plt.plot((r_AU[:,index])[index_h],(dvh[:,index]*dth[:,index]*3600*24/D_Earth)[index_h],'g',label='Hohmann') #Hohmann
    plt.plot((r_AU[:,index])[index_be],(dvbe[:,index]*dtbe[:,index])[index_be]*3600*24/D_Earth,'b',label='Bi-elliptic') #Bi-elliptic
    plt.plot(D_Mars_AU*np.ones(dvh_red.shape),dvh_red*dth_red*3600*24/D_Earth,'r',linestyle='-.',label='Mars') #Mars orbit
    plt.plot(D_Ceres_AU*np.ones(dvh_red.shape),dvh_red*dth_red*3600*24/D_Earth,'c',linestyle='--',label='Ceres') #Ceres orbit
    plt.plot(D_Jupiter_AU*np.ones(dvh_red.shape),dvh_red*dth_red*3600*24/D_Earth,'y',linestyle='-',label='Jupiter') #Jupiter orbit
    plt.plot(D_Saturn_AU*np.ones(dvh_red.shape),dvh_red*dth_red*3600*24/D_Earth,'m',linestyle=':',label='Saturn') #Saturn orbit
    plt.xlabel('Outer Orbit Radius (AU)')
    plt.ylabel('Displacement (AU)')
    plt.title('Quantifying Limits')
    plt.legend(loc='lower right',numpoints=1)
    plt.show()