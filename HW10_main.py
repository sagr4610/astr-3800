"""
Sara Grandone
Homework 10
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import chi2


plt.close('all')

# Define constants
h = 6.626e-34 #plank constant
k = 1.380649e-23 #Boltzmann Constant
c = 3.e8 #speed of light in meters/second 

# Load data file
data = np.loadtxt('correl10.txt',float)
lam = np.array(data[:,0]) #A
Counts = np.array(data[:,1])

# Make Norm function and apply to data
NormFun = lambda x: (x)/(x.max())
Cnorm = NormFun(Counts)
 
# Make temperature vector
Temp = 10.**np.linspace(5.,8.,1000) 

# Define blackbody function
BB = lambda lam,T: 2.*c/(lam**5*(np.exp(h*c/(lam*k*T))-1.))

# Define Normalized Black Body function
BBNorm = lambda lam,T: NormFun(BB(lam,T))

# Define the FF function
#FF = lambda lam,T: np.exp(-h*c/(W*k*T))/(W)

# Define Bremstrahlung function 
Brem = lambda lam,T: h*c*np.exp(-c*h/(k*lam*T))/(k*T*lam**2)

# Define Normalized Bremstrahlung Function
BremNorm = lambda lam,T: NormFun(Brem(lam,T))

#Define Normalized Free Free Function
#NormFF = lambda lam,T: norm(FF(W,T))

# Define Chi Squared function
Chi2 = lambda cval,bval: ((cval-bval)**2/bval).sum()

# Calculate chi2
BBchi2 = np.empty(Temp.shape)
# chi2FF = np.empty(Temp.shape)

BremChi2 = np.empty(Temp.shape)

for i,T in enumerate(Temp):
    BBchi2[i] = Chi2(Cnorm+1,BBNorm(lam*1.e-10,T)+1.)
    #chi2FF[i] = chi2(Fnorm+1,NormFF(lam/1e10,T)+1)
    BremChi2[i] = Chi2(Cnorm+1,BremNorm(lam*1.e-10,T)+1.)


# Minimum Temperatures for Minimum chi2s
BB_ChiMin,BB_TempMin = BBchi2.min(), BBchi2.argmin()
#FF_ChiMin,FF_TempMin = chi2FF.min(), chi2FF.argmin()
Brem_ChiMin,Brem_TempMin = BremChi2.min(), BremChi2.argmin() 

print('Chi^2 Min of BB: %8.3f \n' %BB_ChiMin)
print('Min Temp at Chi^2 Min of BB: %8.3f \n' %Temp[608])

#print('Chi^2 Min of Free Free: %8.3f' %FF_ChiMin)
#print('Min Temp at Chi^2 Min of Free Free: %8.3f' %Temp[856])

print('Chi^2 Min of Brem: %8.3f \n' %Brem_ChiMin)
print('Min Temp at Chi^2 Min of Brem: %8.3f \n' %Temp[856])


# Plot best fit of temp
plt.plot(lam,BBNorm(lam/1e10,Temp[BB_TempMin]), label = 'BB')
#plt.plot(lam,NormFF(lam/1e10,Temp[FF_TempMin]), label = 'FF')
plt.plot(lam,Cnorm,'ro', label = 'lam vs \nNorm \nCounts')
plt.plot(lam,BremNorm(lam/1e10,Temp[Brem_TempMin]),'k', label = 'Brem')
plt.legend(loc = 'upper right')
plt.xlabel('Temperature (K)')
plt.xscale('linear')
plt.ylabel('Normalized Counts/Wavelength')
plt.title('BB and Brem Functions of Wavelength')
plt.show()

y = Brem(lam/1e10,Temp[Brem_TempMin]).max()
x = Counts.max()
fix = x/y

# Adjusted Brem model
plt.figure()
plt.plot(lam,fix*Brem(lam/1e10,Temp[Brem_TempMin]), 'k',label = 'Brem')
plt.plot(lam,Counts,'ro', label = 'Data')
plt.legend(loc = 'upper right')
plt.xlabel('Temperature (K)')
plt.xscale('linear')
plt.ylabel('Counts')
plt.title('Comparing Brem to Data')
plt.show()

# Results
print('BB spectrum model: "horseshoes and hand grenades" model \n')
print('FF Bremsstrahlung spectrum model- Good Fit \n')

#Temp range
Correct = np.linspace(.8*fix,1.2*fix,100)
Temps = np.linspace(.8*Temp[Brem_TempMin],1.2*Temp[Brem_TempMin],100)

# Preallocate space
Chi = np.zeros((100,100))

for i,l in enumerate(Temps):
    for j,m in enumerate(Correct):
        Brem_fit = Brem(lam/1e10,l)*m
        Chi[i,j] = ((Counts-Brem_fit)**2/Brem_fit).sum() 

# Find confidence
Confidence = chi2.cdf(Chi,99)

# Plot Confidence
plt.figure()
CP = plt.contour(Confidence, levels = [0.65, 0.95, 0.995])
plt.legend()
plt.xlabel('Temperature (K)')
plt.xscale('linear')
plt.ylabel('Confidence Values')
plt.title('Contour Plot of Confidences')

labels = ['65% Confidence', '95% confidence','99.5% Confidence']
for i in range(len(labels)):
    CP.collections[i].set_label(labels[i])

plt.legend(loc='upper right')
plt.show()


# Marginalization
plt.figure()
M_x = Chi.sum(axis = 0)
M_y = Chi.sum(axis = 1)
plt.plot(M_x)
plt.ylabel('Marginalized Chi')
plt.xlabel('Temperatures (K)')
plt.title('Marginalized Plot')
plt.show()