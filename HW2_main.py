import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
import os

# Get ride of any plots that may be up
plt.close("all")

# Define x, y, and dy vectors given
x = [1.,2.,3.,4.]
y = [1.,7.,3.2,9.7]
dy= [.1,.2,.15,.3]

# Create plot
plt.plot(x,y,'D')
plt.errorbar(x,y,xerr=0,yerr=dy,linestyle='None')

# Make some labels
plt.xlim(0.,5.)
plt.ylim(0.,10.5)
plt.xlabel('X-axis')
plt.ylabel('Y-axis')
plt.title('Homework 2 Python Plot')
plt.show()