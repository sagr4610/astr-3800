## HW 4 code for IQ questions

import math

# Known values
mu = 100.
sig = 15.
num_US = 319.e6
num_ever = 107.e9
num_alive = 7.e9

## 1a

# Start IQ iteration at 1 std dev.
IQ_US = mu

# while loop iterates through integration of gaussian until it reaches
# a point where less than one person would have a given IQ and returns
# previous IQ value where there would be one person outside of integral
while num_US*(1-math.erf((IQ_US-mu)/(2**0.5*sig))) > 1.:
    IQ_US += 1
    
print(IQ_US-1)
# Results: The IQ of the smartest person in the US is about 188



## 1b

# Create new IQ variable like before
IQ_ever = mu

# Same while loop as before
while num_ever*(1-math.erf((IQ_ever-mu)/(2**0.5*sig))) > 1.:
    IQ_ever += 1
    
print(IQ_ever-1)
# Results: The IQ of the smartest ever is about 202



## 1c

# Create new IQ variable like before
IQ_alive = mu

# Same while loop as before
while num_alive*(1-math.erf((IQ_alive-mu)/(2**0.5*sig))) > 1.:
    IQ_alive += 1
    
print(IQ_alive-1)
# Results: The IQ of the smartest person alive is calculated to
#          be around 196. This means this woman's claim about 
#          having an IQ of 200 is likely false.
