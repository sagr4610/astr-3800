def binning(Flux,lam,lam_bin,bins):
     
    import numpy as np
    h = 6.626e-34
    c = 3e8 #m/s
    bins = 30

    indices = lam.shape[0]/bins
    flux = []
    
    # Loop that sums flux over each wavelength range for binning
    for i in range(bins):
        flux.append(Flux[i*indices:((i+1)*indices-1)].sum())
    flux = np.array(flux)
    photons = flux/(h*c/lam_bin)
    print(flux.sum())
    return photons