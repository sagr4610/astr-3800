## Homework 6

import numpy as np
import xlsxwriter as xs
import matplotlib.pyplot as plt

# Read in file
# file = open('rays.txt','r')
data = np.loadtxt('rays.txt',float)


#for line in open('rays.txt'):
#    new_data = np.array((data.float(i) for i in line.split(' ')))
#    data.append(new_data)

# Extract columns
x = data[:,0]
y = data[:,1]
z = data[:,2]
qx = data[:,3]
qy = data[:,4]
qz = data[:,5]

shape = x.shape

# Find averages and x-x_av, y-y_av
x_av = np.mean(x)
y_av = np.mean(y)

dx = x-x_av*np.ones(shape)
dy = y-y_av*np.ones(shape)

# Plot things
plt.close('all')
plot = plt.plot(dx,dy,'.')
plt.xlabel('x - x_av')
plt.ylabel('y - y_av')
plt.title('Homework 6: Sara Grandone')
plt.xlim((np.min(dx)-.1*np.min(dx),np.max(dx)+.1*np.max(dx)))
plt.ylim((np.min(dy)-.1*np.min(dy),np.max(dy)+.1*np.max(dy)))
plt.show()

# Truncating
N = 25
dx25 = dx[0:(N-1)]
dy25 = dy[0:(N-1)]

d25 = np.transpose(np.vstack([dx25,dy25]))

# Export to excel
workbook = xs.Workbook('Homework6.xlsx')
worksheet = workbook.add_worksheet()
bold = workbook.add_format({'bold': 1})

worksheet.write('A1','x - x_av',bold)
worksheet.write('B1','y - y_av',bold)

row = 1
col = 0

for xval,yval in (d25):
    worksheet.write(row,col,xval)
    worksheet.write(row,col+1,yval)
    row+=1
    
workbook.close()