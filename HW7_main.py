"""
Sara Grandone
Homework 7

"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import pearsonr as pr

### Read in data
x,y = np.loadtxt('correl.txt', unpack = True)
#___________________________________________________ 
 
### Fit the data
num = y.shape[0]
D = num*np.sum(x**2)-(np.sum(x)**2)
A = (np.sum(x**2)*np.sum(y)-np.sum(x)*np.sum(x*y))/D
B = (num*np.sum(x*y)-np.sum(x)*np.sum(y))/D
#print ('x: %3.3f' % A)
#print ('y: %3.3f' % B)
 
# New fitted value
yfit = A+B*x
#___________________________________________________ 
 
# Averages, std. dev and correlation
x_av = np.mean(x)
y_av = np.mean(y)
sig = np.sum((x-x_av)*(y-y_av))

bottom = np.sqrt(np.sum((x-x_av)**2)*np.sum((y-y_av)**2))
Corr_calc = sig/bottom
print ('Corr Calc: %3.3f' % Corr_calc)

# Results: Correlation was .931 like in plot (very close to 1)
 
# Check to verify correlation
corr,p = pr(x,y)
print(p)
#________________________________________________________ 
 
### Plot things
plt.close('all')
plt.figure()
plt.plot(x,y,'bo')
plt.plot(x,yfit,'k')
plt.xlabel('x')
plt.ylabel('y')
plt.title('Fitting of Data for Homework 7')
plt.annotate('A = %3.3f' % A, xy=(2.5, 27))
plt.annotate('B = %3.3f' % B, xy=(2.5, 25))
plt.annotate('Corr. = %3.3f' % corr, xy=(2.5, 23))
plt.show()