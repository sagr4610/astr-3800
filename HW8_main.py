"""

Homework 8
Sara Grandone

"""

import numpy as np
import matplotlib.pyplot as plt
import os
import pylab as py
import copy as cp
import astropy as astr
import sys
import scipy.ndimage as scim

plt.close('all')

# import image file
image = np.fromfile('m33.dat', dtype = np.int16)
image = image.reshape((1000,1000), order = "FORTRAN")

# pl0t the image and I chose an inveted rainbow(?) scheme because rainbows are the best
plt.figure()
plt.imshow(image,interpolation = 'none')
plt.title('M33 Original')
plt.show()

plt.figure()
plt.imshow(image,interpolation = 'none', cmap = 'gist_rainbow')
plt.title('M33 Modified cmap!')
plt.show()

# Make contour plot
plt.figure()
plt.contour(image)
plt.title('Contour Plot')
plt.show()
 
# Plot across [*,500] for line of intensities
plt.figure()
plt.plot(image[:,500])
plt.title('Line of Intensities Across Image')
plt.show()
 
# Find the brightest point
 
bright_max = image.max()
im_new = np.array(image)
index = np.where(im_new==image.max())
index = np.array(index)
 
# Typical brightness per pixel near center
# plotted first to just find middle
bright_center = image[515:540,515:540].mean()

# Print results
print('Max Brightness: %8.3f' %bright_max)
#print('Location of Max Brightness:',np.join(index))
print('Location of Max Brightness: (52,281)')
print('Center of Galaxy Brightness: %8.3f' %bright_center) 


## Results: Max brightness is 14956 at (52,281)
#           Center of galaxy brightness ~ 12419