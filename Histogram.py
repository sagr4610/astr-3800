def histo(Flux):
    
    import numpy as np
    import matplotlib.pyplot as plt

    # Generate and plot histogram
    plt.figure()
    num_counts, Bin, p = plt.hist(Flux, bins = 30)
    bin_vec = np.arange(1,31,1)
    plt.bar(bin_vec,num_counts)
    plt.title('Counts Per Bin')
    plt.xlabel('Number of Bins')
    plt.ylabel('Counts per Bin (out of 3000 counts)')
    plt.show()
    
    return num_counts