# Import Statements
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D # This is step 1 to plotting in 3D
import pyfits as pf
import pdb # This will allow us to pause as we go


# Reading in the data
data, hdr = pf.getdata( 'example.fits', header=True)


# Looking at the data in 2D
plt.imshow(data)
plt.show() #To see dimmer things you can take log


# Grabbing a smaller slice to work with 
slice = data[200:400,200:400]
plt.imshow(np.log(slice))
plt.show()
#pdb.set_trace()


# Looking at the data in 1D (collapsing arrays)
collapse = slice.sum(1)


# Looking at the data in 1D (histograms)



# Looking at the data in 3D 



# Grabbing a single star
ind = np.where(slice == slice.max())
star = slice[ind[0]-15:ind[0]+16,ind[1]-15:ind[1]+16]


# Centering the array



# Looking at the star in 1, 2, 3D

x, y =np.meshgrid(np.arange(star.shape[0]),
                    np.arange(star.shape[1]))

fig = plt.figure()
ax = Axes3D(fig)
ax.plot_surface
ax.plot_surface(x,y,star,rstride=1,cstride=1,cmap='jet')

# Saving the star



# Making an aperture

star -= star.min()

# Applying the aperture to the star

radius = 7
center = [31/2.,31/2.]
aper = np.zeros(star.shape)
for i in np.arange(0,2*np.pi,np.pi/100):
    for j in xrange(0,radius+1,1):
        aper[center[0]+j*np.sin(i),center[1]+j*np.cos(i)] = 1

# Looking at the new array



# Reporting the counts and uncertainty