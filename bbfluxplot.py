#This function calculates and plots blackbodies
def bbflux(T,R,d,lam):
    import numpy as np

    # Define constants
    h = 6.626e-34
    c = 3.e8
    k = 1.38065e-23

    # Calculate Flux at surface
    Flux_surface = 4*np.pi*2.*h*c**2/(lam**5*(np.exp(h*c/(lam*k*T))-1))
    
    # Calculate luminosity then flux at 10 pc (I made all of the distances 10 pc)
    Lum = Flux_surface*(4.*np.pi*R**2)*(lam[1]-lam[0])
    Flux = Lum/(4*np.pi*d**2)/(lam[1]-lam[0])
    return Flux
    

#This function plots blackbody curves (flux vs. wavelength)
def bbplot(Flux,Temp,Lam):
    import matplotlib.pyplot as plt
    import numpy as np
    import pdb
    
    Lam = Lam*10**9; #Converts to nanometers
   
    # Plot
    plot = plt.plot(Lam,Flux)
    plt.xlabel('Wavelength (nm)')
    plt.ylabel('Flux W/m^2')
    plt.title('Blackbody Plot')
    
    # Based on temperature, plots as certain color
    if Temp <= 4000.:
        plt.setp(plot,color = 'r')
    if (Temp <= 4800.) & (Temp > 4000.):
        plt.setp(plot,color = [255,140,0])
    if (Temp <= 5400.) & (Temp > 4800.):
        plt.setp(plot,color = 'y')
    if (Temp <= 6000.) & (Temp > 5600.):
        plt.setp(plot,color = 'k')
    if (Temp <= 6600.) & (Temp > 6000.):
        plt.setp(plot,color = 'c')
    if Temp > 6600.:
        plt.setp(plot,color = 'b')

    return