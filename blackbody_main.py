import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
import os


##### THIS IS HOMEWORK 3

plt.close('all')
#This is the main script for the blackbody assignment
#This is the main script for the blackbody assignment
h = 6.626e-34
Rsol = 6.95e8 #m
pc_to_m = 3.086e16
c = 3e8 #m/s
iterate = 3000

# Create wavelength vector
lam_start = 1.e-7
lam_stop = 4.e-6
lam = np.linspace(lam_start,lam_stop,iterate)
f = c/lam

#Sirius A
Temp_Sir = 9940 #K
Radius_Sir = 1.711*Rsol #m
Dist_Sir_pc = 10 #pc
Dist_Sir = Dist_Sir_pc*pc_to_m

#Antares A
Temp_Ant = 3400 #K
Radius_Ant = 883*Rsol #m
Dist_Ant_pc = 10 #pc
Dist_Ant = Dist_Ant_pc*pc_to_m

#Polaris A
Temp_Pol = 6015 #K
Radius_Pol = 46*Rsol #m
Dist_Pol_pc = 10 #pc
Dist_Pol = Dist_Pol_pc*pc_to_m

#Bellatrix
Temp_Bel = 22000 #K
Radius_Bel = 6*Rsol #m
Dist_Bel_pc = 10 #pc
Dist_Bel = Dist_Bel_pc*pc_to_m

# Call bbflux to calculate flux
from bbfluxplot import bbflux
Flux_Sir = bbflux(Temp_Sir,Radius_Sir,Dist_Sir,lam)
Flux_Ant = bbflux(Temp_Ant,Radius_Ant,Dist_Ant,lam)
Flux_Pol = bbflux(Temp_Pol,Radius_Pol,Dist_Pol,lam)
#Flux_Bel = bbflux(Temp_Bel,Radius_Bel,Dist_Bel)

# Call bbplot to plot 
from bbfluxplot import bbplot
bbplot(Flux_Sir,Temp_Sir,lam)
bbplot(Flux_Ant,Temp_Ant,lam)
bbplot(Flux_Pol,Temp_Pol,lam)
#bbplot(Flux_Bel,Temp_Bel)

plt.show()




####### THIS IS HOMEWORK 4

## Let's look at the Sun

# Sun
Temp_Sun = 5778 #K
Radius_Sun = Rsol #m
Dist_Sun_pc = 1000 #pc
Dist_Sun = Dist_Sun_pc*pc_to_m
Flux_Sun = bbflux(Temp_Sun,Radius_Sun,Dist_Sun,lam)
plt.figure()
bbplot(Flux_Sun,Temp_Sun,lam)


# Generate and plot histogram
from Histogram import histo
num_counts = histo(Flux_Sun)

# Results of Lumnosity: Sun's luminosity was found to be 1.5e27 Watts (3 times actual value)




###### THIS IS HOMEWORK 5


#Call function that does binning
from HW5binning import binning
bins = 30
dlam = (lam_stop-lam_start)/bins
lam_bin = np.linspace(lam_start+dlam/2,lam_stop-dlam/2,bins)*1e9
photons_sun = binning(Flux_Sun, lam, lam_bin, bins)  
                    
plt.figure()
plot2 = plt.bar(lam_bin,photons_sun, width = dlam*1e9)
plt.title('Photons/m^2/s vs. Wavelength (nm)')
plt.xlabel('Wavelength (nm)')
plt.ylabel('Photons/m^2/s')
plt.show()

flux = photons_sun*(h*c/lam_bin)
lum = flux.sum()*(4.*np.pi*Dist_Sun**2)*(lam[1]-lam[0])
print(lum)
# Still gives 1.5e27 -check!

# Hubble Part
Diam = 2.4 #m
Ob = .2
Tput = .3
time = 1000 #sec
area = np.pi*Diam**2/4
area_eff = Ob*Tput*area
new_counts = photons_sun*area_eff*time
# Make noise have standard deviation of 1/10 of actual data
noise = .1*np.std(new_counts)*np.random.normal(size = new_counts.shape)
new_counts += noise

plt.figure()
plt.bar(lam_bin,new_counts,width = dlam*1e9)
plt.title('1000 s exposure of Sun at 1000 pc')
plt.xlabel('Wavelength (nm)')
plt.ylabel('Counts')
plt.show()
        