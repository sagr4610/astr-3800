import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
import os


##### THIS IS HOMEWORK 3
#This is the main script for the blackbody assignment
h = 6.626e-34
Rsol = 6.95e8 #m
pc_to_m = 3.086e16
c = 3.e8 #m/s

# Create wavelength vector
lam_start = 1.e-7
lam_stop = 1.e-6
lam = np.linspace(lam_start,lam_stop,1000)
f = c/lam

#Sirius A
Temp_Sir = 9940 #K
Radius_Sir = 1.711*Rsol #m
Dist_Sir_pc = 2.64 #pc
Dist_Sir = Dist_Sir_pc*pc_to_m

#Antares A
Temp_Ant = 3400 #K
Radius_Ant = 883*Rsol #m
Dist_Ant_pc = 170 #pc
Dist_Ant = Dist_Ant_pc*pc_to_m

#Polaris A
Temp_Pol = 6015 #K
Radius_Pol = 46*Rsol #m
Dist_Pol_pc = 100 #pc
Dist_Pol = Dist_Pol_pc*pc_to_m

#Bellatrix
Temp_Bel = 22000 #K
Radius_Bel = 6*Rsol #m
Dist_Bel_pc = 250 #pc
Dist_Bel = Dist_Bel_pc*pc_to_m

# Call bbflux to calculate flux
from bbfluxplot import bbflux
Flux_Sir = bbflux(Temp_Sir,Radius_Sir,Dist_Sir,lam)
Flux_Ant = bbflux(Temp_Ant,Radius_Ant,Dist_Ant,lam)
Flux_Pol = bbflux(Temp_Pol,Radius_Pol,Dist_Pol,lam)
Flux_Bel = bbflux(Temp_Bel,Radius_Bel,Dist_Bel,lam)

# Call bbplot to plot 
from bbfluxplot import bbplot
bbplot(Temp_Sir,Flux_Sir)
bbplot(Temp_Ant,Flux_Ant)
bbplot(Temp_Pol,Flux_Pol)
#bbplot(Temp_Bel,Flux_Bel)


####### THIS IS HOMEWORK 4

## Let's look at the Sun

# Sun
Temp_Sun = 5778 #K
Radius_Sun = Rsol #m
Dist_Bel_Sun = 10 #pc
Dist_Sun = Dist_Sun_pc*pc_to_m
Flux_Sun = bbflux(Temp_Sun,Radius_Sun,Dist_Sun,lam)

# Calculate counts/m^2/s
k = Flux_Sun/(h*f)
bins = 30
dlam = (lam_stop-lam_start)/bins

#Counts_bin = np.zeros(