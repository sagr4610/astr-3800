import numpy as np
import pyfits as pf
import matplotlib.pyplot as plt
import os

os.chdir('/Users/saragrandone/Documents/Python/brbordwell-astr3800-47cea2a927b6/')


#Reading in image
img,hdr = pf.getdata('6044_20111018_Capella_0.fits.gz',0,header=True)

img = img[:,0:256]
img_c = img.sum(0) # collapsed

#Read in my deformation profile
deform = np.genfromtxt('6044_20111018_Capella_left_deform.txt',dtype = float).T
deform[1]=deform[1]-deform[1].min()

#Create padding

size = img.shape
pad = np.zeros((size[0],size[1],deform[1].max()))
padded = np.hstack((pad,img_c,pad))

# Perform shift
for i in xrange(size[1]):
    padded[i] = np.roll(padded[i],int(-1*deform[1,i]))
        
#Cut off padding
zeroes = padded != 0
check = zeroes.sum(0)
ind = np.where(check == 512)

img = padded[:,inds]
img = img.reshape(size[1],img.shape[2])