import numpy as np
import matplotlib.pyplot as plt

def deltav(D,Hoh,Bi):

    ### Define Constants (SI units)
    G = 6.674e-11
    M_Sun = 1.989e30
    D_Earth = 1.496e11
    mu_Sun = G*M_Sun
    it = 100 # Number of iterations
    v0 = np.sqrt(mu_Sun/D_Earth)
    D_Mars = 2.253e11
    ################################
    
    ### Define Functions
    
        # Important Variables for Eqns
    alpha = lambda r1,r2: r2/r1
    ra2 = lambda r1,e: r1*(2/(1-e)-1)
    beta = lambda r1,ra2: ra2/r1
    
        #Bi-elliptic
    dvBE = lambda a,b: v0*(np.sqrt(2*(a+b)/a/b)-(1+np.sqrt(a))/np.sqrt(a)-np.sqrt(2/(b*(1+b)))*(1-b))

        # Hohmann
    dvH = lambda a: v0*(1/np.sqrt(a)-np.sqrt(2)*(1-a)/np.sqrt(a*(1+a))-1)
    
    ##################################
    
    ### Define Constraints
    Out = 20
    emin = (D_Mars-D_Earth)/(D_Mars+D_Earth)
    emax = (D*Out-D_Earth)/(D*Out+D_Earth)
    e_vec = np.linspace(emin,emax,it)
    dist_vec = np.linspace(D_Earth,D*Out/2,it)
    one_arr = np.ones([it,it])
    
    ### Generate Arrays for 2-variable dependance
    e,r = np.meshgrid(e_vec,dist_vec)
    Alpha = alpha(D_Earth*one_arr,r)
    RA = ra2(D_Earth*one_arr,e)
    Beta = beta(D_Earth*one_arr,RA)
    
    ### Calculate dv's and dt's
    if Hoh:
        dv = dvH(Alpha)
    if Bi:
        dv = dvBE(Alpha,Beta)
        

    return dv
    
    
#######################################################
def deltat(D,Hoh,Bi):

    ### Define Constants (SI units)
    G = 6.674e-11
    M_Sun = 1.989e30
    D_Earth = 1.496e11
    mu_Sun = G*M_Sun
    it = 100 # Number of iterations
 
    ################################
    
    ### Define Functions
    
        # Important Variables for Eqns
    ra2 = lambda r1,e: r1*(2/(1-e)-1)
    
        #Bi-elliptic
    tBE = lambda r1,r2,ra2,mu: np.sqrt(np.pi**2*(r1+ra2)**3/8/mu)/3600/24+np.sqrt(np.pi**2*(r2+ra2)**3/8/mu)/3600/24 #days
    
        # Hohmann
    tH = lambda r1,r2,mu: np.sqrt(np.pi**2*(r1+r2)**3/(8*mu))/3600/24 #Days
    
    ##################################
    
    ### Define Constraints
    Out = 20
    emin = (D-D_Earth)/(D+D_Earth)
    emax = (D*Out-D_Earth)/(D*Out+D_Earth)
    e_vec = np.linspace(emin,emax,it)
    dist_vec = np.linspace(D_Earth,D*Out/2,it)
    one_arr = np.ones([it,it])
    
    ### Generate Arrays for 2-variable dependance
    e,r = np.meshgrid(e_vec,dist_vec)
    RA = ra2(D_Earth*one_arr,e)
    
    ### Calculate dv's and dt's
    if Hoh:
        dt = tH(D_Earth*one_arr,r,mu_Sun*one_arr)
    if Bi:
        dt = tBE(D_Earth*one_arr,r,RA,mu_Sun*one_arr)
        

    T = np.sqrt(4*np.pi**2*D**3/mu_Sun)/3600/24;
    ang = dt/T*360-np.floor(dt/T)*360;
    
    alpha = 180-ang

    return (dt,alpha)
    
    
        
    
        