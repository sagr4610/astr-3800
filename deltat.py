import numpy as np
import matplotlib.pyplot as plt


def deltav(D,Hoh,Bi):
    

    plt.close('all')

    ### Define Constants (SI units)
    G = 6.674e-11
    M_Sun = 1.989e30
    D_Earth = 1.496e11
    mu_Sun = G*M_Sun
    it = 100 # Number of iterations
 
    ################################
    
    ### Define Functions
    
        # Important Variables for Eqns
    ra2 = lambda r1,e: r1*(2/(1-e)-1)
    
        #Bi-elliptic
    tBE = lambda r1,r2,ra2,mu: np.sqrt(np.pi**2*(r1+ra2)**3/8/mu)/3600/24+np.sqrt(np.pi**2*(r2+ra2)**3/8/mu)/3600/24 #days
    
        # Hohmann
    tH = lambda r1,r2,mu: np.sqrt(np.pi**2*(r1+r2)**3/(8*mu))/3600/24 #Days
    
    ##################################
    
    ### Define Constraints
    Out = D*3
    emin = (D-D_Earth)/(D+D_Earth)
    emax = (D_Earth*Out-D_Earth)/(D_Earth*Out+D_Earth)
    e_vec = np.linspace(emin,emax,it)
    dist_vec = np.linspace(D_Earth,D_Earth*Out,it)
    one_arr = np.ones([it,it])
    
    ### Generate Arrays for 2-variable dependance
    e,r = np.meshgrid(e_vec,dist_vec)
    RA = ra2(D_Earth*one_arr,e)
    
    ### Calculate dv's and dt's
    if Hoh:
        dt = tH(D_Earth*one_arr,r,mu_Sun*one_arr)
    if Bi:
        dt = tBE(D_Earth*one_arr,r,RA,mu_Sun*one_arr)
        

    return dt
    
    
        