def deltav(D,Hoh,Bi):
    
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.optimize import fsolve
    import pylab
    from matplotlib.patches import Arc

    plt.close('all')

    ### Define Constants (SI units)
    G = 6.674e-11
    M_Sun = 1.989e30
    D_Earth = 1.496e11
    mu_Sun = G*M_Sun
    it = 100 # Number of iterations
    v0 = np.sqrt(mu_Sun/D_Earth)
    ################################
    
    ### Define Functions
    
        # Important Variables for Eqns
    alpha = lambda r1,r2: r2/r1
    ra2 = lambda r1,e: r1*(2/(1-e)-1)
    beta = lambda r1,ra2: ra2/r1
    
        #Bi-elliptic
    dvBE = lambda alpha,beta: v0*(np.sqrt(2*(alpha+beta)/alpha/beta)-(1+np.sqrt(alpha))/np.sqrt(alpha)-np.sqrt(2/(beta*(1+beta)))*(1-beta))

        # Hohmann
    dvH = lambda alpha: v0*(1/np.sqrt(alpha)-np.sqrt(2)*(1-alpha)/np.sqrt(alpha*(1+alpha))-1)
    
    ##################################
    
    ### Define Constraints
    Out = D*3
    emin = (D-D_Earth)/(D+D_Earth)
    emax = (D_Earth*Out-D_Earth)/(D_Earth*Out+D_Earth)
    e_vec = np.linspace(emin,emax,it)
    dist_vec = np.linspace(D_Earth,D_Earth*Out,it)
    one_arr = np.ones([it,it])
    
    ### Generate Arrays for 2-variable dependance
    e,r = np.meshgrid(e_vec,dist_vec)
    Alpha = alpha(D_Earth*one_arr,r)
    RA = ra2(D_Earth*one_arr,e)
    Beta = beta(D_Earth*one_arr,RA)
    
    ### Calculate dv's and dt's
    if Hoh:
        dv = dvH(Alpha)
    if Bi:
        dv = dvBE(Alpha,Beta)
        

    return dv
    
    
#######################################################
def deltav(D,Hoh,Bi):
    
    import numpy as np
    import matplotlib.pyplot as plt
    from scipy.optimize import fsolve
    import pylab
    from matplotlib.patches import Arc

    plt.close('all')

    ### Define Constants (SI units)
    G = 6.674e-11
    M_Sun = 1.989e30
    D_Earth = 1.496e11
    mu_Sun = G*M_Sun
    it = 100 # Number of iterations
 
    ################################
    
    ### Define Functions
    
        # Important Variables for Eqns
    ra2 = lambda r1,e: r1*(2/(1-e)-1)
    
        #Bi-elliptic
    tBE = lambda r1,r2,ra2,mu: np.sqrt(np.pi**2*(r1+ra2)**3/8/mu)/3600/24+np.sqrt(np.pi**2*(r2+ra2)**3/8/mu)/3600/24 #days
    
        # Hohmann
    tH = lambda r1,r2,mu: np.sqrt(np.pi**2*(r1+r2)**3/(8*mu))/3600/24 #Days
    
    ##################################
    
    ### Define Constraints
    Out = D*3
    emin = (D-D_Earth)/(D+D_Earth)
    emax = (D_Earth*Out-D_Earth)/(D_Earth*Out+D_Earth)
    e_vec = np.linspace(emin,emax,it)
    dist_vec = np.linspace(D_Earth,D_Earth*Out,it)
    one_arr = np.ones([it,it])
    
    ### Generate Arrays for 2-variable dependance
    e,r = np.meshgrid(e_vec,dist_vec)
    RA = ra2(D_Earth*one_arr,e)
    
    ### Calculate dv's and dt's
    if Hoh:
        dt = tH(D_Earth*one_arr,r,mu_Sun*one_arr)
    if Bi:
        dt = tBE(D_Earth*one_arr,r,RA,mu_Sun*one_arr)
        

    return dt
    
    
        
    
        