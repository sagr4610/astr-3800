import numpy as np
import matplotlib.pyplot as plt

def red(D,dv,dt,Maxdv,MaxTime,Hoh,Bi):

    ### Relevant info (SI units)
    D_Earth = 1.496e11
    it = 100 # Number of iterations
    Out = 20
    emin = (D-D_Earth)/(D+D_Earth)
    emax = (D*Out-D_Earth)/(D*Out+D_Earth)
    e_vec = np.linspace(emin,emax,it)
    dist_vec = np.linspace(D_Earth,D*Out/2,it)
    
    ### Generate Arrays for 2-variable dependance
    e,r = np.meshgrid(e_vec,dist_vec)
    ################################
    
    ### Make another reduction of eliminating unreasonable dvs and dts
    dt_red = []
    dv_red = []
    r_red = []
    e_red = []
    
    for i in range(it):
        for j in range(it):
            if Hoh & (dt[i,j] < MaxTime) & (dv[i,j] < Maxdv):
                dt_red.append(dt[i,j])
                dv_red.append(dv[i,j])
	        r_red.append(r[i,j])
        	e_red.append(e[i,j])
            if Bi & (dt[i,j] < MaxTime) & (dv[i,j] < Maxdv):
                dt_red.append(dt[i,j])
                dv_red.append(dv[i,j])
                r_red.append(r[i,j])
                e_red.append(e[i,j])
    
    dt_red = np.array(dt_red)
    dv_red = np.array(dv_red)
    r_red = np.array(r_red)
    e_red = np.array(e_red)

    return (dt_red,dv_red,r_red,e_red)